#!/usr/bin/env julia
# Project Euler Problem #10
"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
"""
function isPrime(n)
    if n < 2
        return false
    elseif n == 2
        return true
    elseif n % 2 ==0
        return false
    else 
        x = 3    
        while x < sqrt(n)+1
            if n % x == 0
                return false
            end
            x+=2
        end    
        return true
    end    
end
 
prime_sum = 0
for i in 1:2000000
    if isPrime(i)
        prime_sum += i
    end
end
                   
println("The sum of all the primes below two million is: $(prime_sum)")

  
 

  
  



