#!/usr/bin/env python
# Project Euler Problem #10
"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
"""
def isPrime(n):
    import math
    if n < 2:
        return False
    elif n == 2: 
        return True
    elif n % 2 ==0:
        return False
    else: 
        x = 3    
        while x < int(math.sqrt(n))+1:
            if n % x == 0:
                return False
            x+=2
        return True

if __name__ == "__main__":
  
    prime_sum = sum([i for i in xrange(1,2000000) if isPrime(i)])
                       
    print "The sum of all the primes below two million is: %s" %(prime_sum)

  
 

  
  



