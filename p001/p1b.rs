// Project Euler Problem #1

fn main() {

	let mut sum = 0;
	
	for i in range(3,1000) { 
		if i % 3 ==0 || i % 5 == 0 {
			sum += i;
		}
    } 
 	println!("The sum of all natural numbers <1000 and divisible by 3 or 5 is: {}", sum);
}
