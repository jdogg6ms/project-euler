//Test Primes
//Prime2 is faster by 50%


package main

import (
    "fmt"
	"math"
)

func isPrime(n float64) bool {
	if n < 2 {
		return false
	}
	if n == 2 {
		return true
	}
	if math.Mod(n, 2) ==0 { 
	 return false
	}
	var x float64 = 3
	limit := math.Sqrt(n)
	for x <= limit {
		if math.Mod(n, x) == 0 {
			return false
		}
		x += 2
	}
	return true
}

func isPrime2(num float64) bool {
	n := int(num)
	if n < 2 {
		return false
	}
	if n == 2 {
		return true
	}
	if n % 2 ==0 { 
	 return false
	}
	x := 3
	limit := int(math.Sqrt(num))
	for x <= limit {
		if n % x == 0 {
			return false
		}
		x += 2
	}
	return true
}
/*
func sundaram(n int) []int { 
    primes []int
    numbers = range(3, max_n+1, 2)
    half = (max_n)//2
    initial = 4

    for step in xrange(3, max_n+1, 2):
        for i in xrange(initial, half, step):
            numbers[i-1] = 0
        initial += 2*(step+1)

        if initial > half:
            return [2] + filter(None, numbers)
*/
func main() {
    count := 0
	for i := 5.0; i < 100000000; i+=2 {
        if isPrime2(i) {count +=1}
	}
	fmt.Println(count)
}

