#!/usr/bin/env julia
# Project Euler Problem #3


# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143?


function isPrime(n)
    if n < 2
        return false
    elseif n == 2
        return true
    elseif n % 2 ==0
        return false
    end
    x = 3
    limit = int(sqrt(n))+1
    while x < limit
        if n % x == 0
            return false
        end
        x+=2
    end
    return true
end
    
    result = 1
    num = 600851475143
    sqrt_num = int(sqrt(num)+1)

    for i in [1:2:sqrt_num]
        if num % i == 0
            factor = num / i
            if isPrime(i)
                result = maximum([i,result])
            end    
            if isPrime(factor)
                result = maximum([factor,result])
            end
        end
    end

    if result == 1
        result = num
    end

    println("The largest prime factor of $(num) is: $(result)")